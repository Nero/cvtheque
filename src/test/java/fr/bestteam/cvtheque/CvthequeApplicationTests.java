package fr.bestteam.cvtheque;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fr.bestteam.cvtheque.bll.CompetenceManager;
import fr.bestteam.cvtheque.bll.ManagerException;
import fr.bestteam.cvtheque.bll.MotCleManager;
import fr.bestteam.cvtheque.bll.PersonneManager;
import fr.bestteam.cvtheque.bo.Competence;
import fr.bestteam.cvtheque.bo.Experience;
import fr.bestteam.cvtheque.bo.Formation;
import fr.bestteam.cvtheque.bo.MotCle;
import fr.bestteam.cvtheque.bo.Personne;
import fr.bestteam.cvtheque.bo.Remarque;


@RunWith(SpringRunner.class)
@SpringBootTest
public class CvthequeApplicationTests {

	@Autowired
	PersonneManager mgrPers;
	
	@Autowired
	CompetenceManager mgrComp;
	
	@Autowired
	MotCleManager mgrMotcle;
	
	@Test
	@Transactional
	public void contextLoads() {
		
		/////////////////////
		// INITIALISATION
		/////////////////////
		
		List<Personne> personnes = new ArrayList<>();
			
		Personne p1 = new Personne(
				"NOM1","Prenom1","Mr", "nom1.prenom1@mail.fr",
				"06-01-02-03-04","1 rue de l'IMIE","ORVAULT","44000",
				"10/10/2002","linkedin","40","permis A, permis B",
				"","","moto, velo, tricycle","Developpeur Java",true);
		
		Personne p2 = new Personne("NOM2","Prenom2","Mme", "nom2.prenom2@mail.fr",
				"06-10-32-03-54","45 b is rue de Brest","ORVAULT","44000",
				"01/11/2000","linkedin","50","permis B",
				"","","moto, jeux videos","Developpeur Java",true);
		
		Personne p3 = new Personne("NOM3","Prenom3","Mme", "nom3.prenom3@mail.fr",
				"02-01-02-03-04","rue du centre","NANTES","44000",
				"29/02/2000","linkedin","30","",
				"","","peche, velo","Developpeur Angular",false);
		
		Personne p4 = new Personne("NOM4","Prenom4","Mme", "nom4.prenom4@mail.fr",
				"01-01-02-03-04","rue de l'IMIE","ORVAULT","44000",
				"25/12/0000","linkedin","24","permis BSR",
				"","","nature, peinture","Developpeur HTML",false);
		
		Personne p5 = new Personne("NOM5","Prenom5","Mr", "nom5.prenom5@mail.fr",		
				"06-01-63-43-04","rue de l'esperence","GUENROUET","44530",
				"23/09/1998","linkedin","80","permis A, permis B",
				"","","","Ingenieur JEE",true);
		
		Personne p6 = new Personne("NOM6","Prenom6","Mme", "nom6.prenom6@mail.fr",
				"06-01-02-75-34","rue de l'�glise","PONTCHATEAU","44630",
				"05/05/1995","linkedin","40","permis A, permis B",
				"","","Basket","Developpeur Java",true);
		
		Experience e1 = new Experience("Scrum Master", "decription1", "01/01/2000","02/01/2000","Entreprise TouTiSeCode");
		Experience e2 = new Experience("Developpeur", "decription2", "01/01/2002","02/01/2010","Entreprise Onpratik a Gilles");
		Experience e3 = new Experience("Technicien", "decription3", "","","Company du code � tonton");
		Experience e4 = new Experience("Bonimenteur", "decription4", "01/01/1975","31/08/2000","Java didecouter");
		Experience e5 = new Experience("Developpeur", "decription5", "","","Spring pour tous");
		Experience e6 = new Experience("Developpeur", "decription6", "01/01/1998","02/01/2019","Hibernate");
		Experience e7 = new Experience("Femme de m�nage", "decription7", "01/01/2018","","PadeMakchezNous");
		Experience e8 = new Experience("Mathematicien", "decription8", "01/01/2018","","P2I");
		Experience e9 = new Experience("Developpeur", "decription9", "01/01/2017","","Toutankhamon");
		Experience e10 = new Experience("Clown", "", "01/01/2000","02/01/2010","Eiffel");

		
		Formation f1 = new Formation("Formation Scrum Master", "decription1", "01/01/2000","02/01/2000","IMIE");
		Formation f2 = new Formation("Formation D�veloppeurJava", "decription2", "01/01/2002","02/01/2010","IMIE");
		Formation f3 = new Formation("Formation JEE Avanc�", "decription3", null,null,"ENI");
		Formation f4 = new Formation("Formation Architecte", "decription4", "01/01/1975","31/08/2000","ENI");
		Formation f5 = new Formation("Formation de savoir vivre", "decription5", null,null,"CNAM");
		Formation f6 = new Formation("L'anti Stress", "decription6", "01/01/1998","02/01/2019","CNAM");
		Formation f7 = new Formation("Trouver du travail chez Pole Emploi", "decription7", "01/01/2018",null,"IMIE");
		Formation f8 = new Formation("Technicien r�seaux", "decription8", "01/01/2018","","ENI");
		
		Remarque r1 = new Remarque("Pourrait devenir responsable de projets","01/06/2019");
		Remarque r2 = new Remarque("A des lacunes sur sping","05/06/2018");
		Remarque r3 = new Remarque("Pourrait devenir SCRUM master","09/10/2017");
		Remarque r4 = new Remarque("Plutôt attirer par le Backend","12/06/2019");
		Remarque r5 = new Remarque("Il aime bien Javascript","01/05/2018");
		Remarque r6 = new Remarque("Référent possible sur Angular","15/06/2017");

		Competence c1 = new Competence("Java");
		Competence c2 = new Competence("Hibernate");
		Competence c3 = new Competence("Spring Boot");
		Competence c4 = new Competence("Tricot");
		Competence c5 = new Competence("Art du Manga");
		Competence c6 = new Competence("Responsable");
		
		MotCle mc1 = new MotCle("Formateur");
		MotCle mc2 = new MotCle("Architecte");
		MotCle mc3 = new MotCle("SCRUM");
		MotCle mc4 = new MotCle("DSI");
		MotCle mc5 = new MotCle("Responsable");
		
		// Ajout d'Exp�rience
		p1.addExperiencePersonne(e1);
		p1.addExperiencePersonne(e7);
		p2.addExperiencePersonne(e2);
		p3.addExperiencePersonne(e3);
		p3.addExperiencePersonne(e4);
		
		// Ajout de Formations
		p1.addFormationPersonne(f1);
		p1.addFormationPersonne(f2);
		p2.addFormationPersonne(f3);
		p2.addFormationPersonne(f4);
		p3.addFormationPersonne(f5);
		
		// Ajout de Remarques
		p1.addRemarque(r1);
		p1.addRemarque(r2);
		p2.addRemarque(r3);
		p2.addRemarque(r4);
		p3.addRemarque(r5);
		
		// Ajout des comp�tences d�j� en BDD
		p1.addCompetence(c1);
		p1.addCompetence(c2);
		p3.addCompetence(c1);
		
		//ajout comp�tence non en BDD
		p3.addCompetence(c3);
		
		// ajout de mot-clé déjà en BDD
		p3.addMotCle(mc1);
		p1.addMotCle(mc1);
		
		// ajout de mot-clé non en BDD
		p3.addMotCle(mc2);
		p4.addMotCle(mc3);
		
		// Ajout de comp�tences pas encore affect�es
		try {
			mgrComp.addCompetence(c1);
			mgrComp.addCompetence(c2);
			mgrComp.addCompetence(c3);
			mgrComp.addCompetence(c4);
		} catch (ManagerException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		
		// Ajout de mots-clés pas encore affectés
		try {
			mgrMotcle.addMotCle(mc4);
			mgrMotcle.addMotCle(mc5);
		} catch (ManagerException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		
		System.out.println("-------------------------------------");
		p1.getExperiences().stream().forEach(item->System.out.println(item));
		System.out.println("-------------------------------------");
		p1.getFormations().stream().forEach(item->System.out.println(item));
		System.out.println("-------------------------------------");		
		
		// TESTS D'AJOUTS DE PERSONNES
			try {
				
				mgrPers.addPerson(p1);
				mgrPers.addPerson(p2);
				mgrPers.addPerson(p3);
				mgrPers.addPerson(p4);
				mgrPers.addPerson(p5);
				mgrPers.addPerson(p6);
				
				
			} catch (ManagerException e) {
				System.err.println(e);
				e.printStackTrace();
			}
			System.out.println("================================");

			// TEST D'AJOUT DE COMPETENCES
			
			try {
				mgrComp.addCompetence(c4);
			} catch (ManagerException e) {
				System.err.println(e);
				e.printStackTrace();
			}
			
			// TESTS D'AFFICHER LA LISTE DE TOUTES LES PERSONNES
			personnes=mgrPers.findAll();
			System.out.println("-------------------------------------");
			personnes.stream().forEach(item->System.out.println(item));
			System.out.println("-------------------------------------");
			
			System.out.println("================================");
			
			// TESTS D'AFFICHER UNE PERSONNE PAR SON ID
			System.out.println(mgrPers.findById(2L));
			
			
			System.out.println("= TEST MODIFICATION ===============================");
			
			// TEST MODIFICATION
			
			Personne pMod = mgrPers.findById(2L);
			// modif du nom
			pMod.setNom("CHAPLINNN");
			// Ajouter une nouvelle experience
			pMod.addExperiencePersonne(e6);
			pMod.addExperiencePersonne(e8);
			pMod.addExperiencePersonne(e9);
			// Ajouter une nouvelle formation
			pMod.addFormationPersonne(f8);
			// Ajouter une nouvelle Remarque
			pMod.addRemarque(r6);
			// Ajouter des comp�tences d�j� dans la base
			pMod.addCompetence(c1);
			pMod.addCompetence(c5);
			// Ajouter des comp�tences non existantes dans la base
			pMod.addCompetence(c6);
			// Ajouter un mot-clé déjà dans la base
			pMod.addMotCle(mc3);
			// Ajouter un mot-clé non existant
			pMod.addMotCle(mc5);
			
			try {
				mgrPers.updatePerson(pMod);
				
			} catch (ManagerException e) {
				System.err.println(e);
				e.printStackTrace();
			}
			
			///////// J'enl�ve la competence c5 � pMod : Elle ne doit se supprimer que de la liste de comp de la personne.
			//pMod.deleteCompetence(c5);
			
			try {
				mgrPers.updatePerson(pMod);
				
			} catch (ManagerException e) {
				System.err.println(e);
				e.printStackTrace();
			}
			
			///////// J'enl�ve le mot-clé mc5 � pMod : Elle ne doit se supprimer que de la liste de la personne.
			//pMod.deleteMotCle(mc5);
			
			try {
				mgrPers.updatePerson(pMod);
				
			} catch (ManagerException e) {
				System.err.println(e);
				e.printStackTrace();
			}
			

			System.out.println("================================");
			// TEST SUPPRESSION
			
			// Les experiences - les activit�s doivent etre supprim�es
			// Les comp�tences ne doivent pas �tre supprim�es
//			try {
//				mgrPers.deleteById(1L);
//			} catch (ManagerException e) {
//				System.err.println(e);
//				e.printStackTrace();
//			}

			System.out.println("================================");
			
			// List
			personnes=mgrPers.findAll();
			personnes.stream().forEach(item->System.out.println(item));
			
			Set<Competence> competences = p2.getCompetences();
			competences.stream().forEach(item->System.out.println(item));
			
	}	
	

}
