package fr.bestteam.cvtheque.dal;

import org.springframework.data.repository.CrudRepository;

import fr.bestteam.cvtheque.bo.Formation;

public interface FormationDao extends CrudRepository<Formation, Long> {

}
