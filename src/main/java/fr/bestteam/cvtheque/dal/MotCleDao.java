package fr.bestteam.cvtheque.dal;

import org.springframework.data.repository.CrudRepository;

import fr.bestteam.cvtheque.bo.MotCle;

public interface MotCleDao extends CrudRepository<MotCle, Long> {

}
