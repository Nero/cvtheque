package fr.bestteam.cvtheque.dal;

import org.springframework.data.repository.CrudRepository;

import fr.bestteam.cvtheque.bo.Remarque;

public interface RemarqueDao extends CrudRepository<Remarque, Long> {

}
