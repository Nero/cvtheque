package fr.bestteam.cvtheque.dal;





import org.springframework.data.repository.CrudRepository;

import fr.bestteam.cvtheque.bo.Personne;
/**
 * Interface CRUD <Personne, Type id>
 * @author CIVEL
 *
 */
public interface PersonneDao extends CrudRepository<Personne, Long>{
	


}
