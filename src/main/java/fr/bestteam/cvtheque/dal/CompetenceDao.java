package fr.bestteam.cvtheque.dal;

import org.springframework.data.repository.CrudRepository;

import fr.bestteam.cvtheque.bo.Competence;

public interface CompetenceDao extends CrudRepository<Competence, Long>{

}
