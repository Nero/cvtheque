package fr.bestteam.cvtheque.dal;
/**
 * Interface CRUD <Experience, Type id>
 * @author CIVEL
 *
 */
import org.springframework.data.repository.CrudRepository;

import fr.bestteam.cvtheque.bo.Experience;

public interface ExperienceDao extends CrudRepository<Experience, Long> {

}
