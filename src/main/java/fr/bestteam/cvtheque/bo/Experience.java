package fr.bestteam.cvtheque.bo;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "experience")
public class Experience implements Serializable{
	/**
	 * Entit� Experience
	 */
	private static final long serialVersionUID = 1L;

	///////////////
	// Attributs
	///////////////
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(nullable = false)
	private String intitule;
	private String description;
	private String dateDebutExperience;
	private String dateFinExperience;
	@Column(nullable = false)
	private String nomEntreprise;
	
	///////////////////
	// Jointures
	///////////////////
	
	@ManyToOne
	@JoinColumn(name = "idPersonne")
	private Personne personne;
	
	///////////////////
	// Constructeurs
	///////////////////
	public Experience() {
	}
	
	public Experience(String intitule, String description, String dateDebutExperience, String dateFinExperience,
			String nomEntreprise) {
		super();
		this.intitule = intitule;
		this.description = description;
		this.dateDebutExperience = dateDebutExperience;
		this.dateFinExperience = dateFinExperience;
		this.nomEntreprise = nomEntreprise;
	}



	public Experience(String intitule, String nomEntreprise) {
		super();
		this.intitule = intitule;
		this.nomEntreprise = nomEntreprise;
	}

	
	//////////////////////
	// GETTERS/SETTERS
	//////////////////////

	public Long getId() {
		return id;
	}


	public String getIntitule() {
		return intitule;
	}



	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public String getDateDebutExperience() {
		return dateDebutExperience;
	}



	public void setDateDebutExperience(String dateDebutFormation) {
		this.dateDebutExperience = dateDebutFormation;
	}



	public String getDateFinExperience() {
		return dateFinExperience;
	}



	public void setDateFinExperience(String dateFinFormation) {
		this.dateFinExperience = dateFinFormation;
	}



	public String getNomEntreprise() {
		return nomEntreprise;
	}



	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
	}
	
	public Personne getPersonne() {
		return personne;
	}



	public void setPersonne(Personne personne) {
		this.personne = personne;
	}


	//////////////////////
	// ToString
	//////////////////////


	@Override
	public String toString() {
		return "Experience [id=" + id + ", intitule=" + intitule + ", description=" + description
				+ ", dateDebutExperience=" + dateDebutExperience + ", dateFinExperience=" + dateFinExperience
				+ ", nomEntreprise=" + nomEntreprise + "]";
	}
	



	
	
}
