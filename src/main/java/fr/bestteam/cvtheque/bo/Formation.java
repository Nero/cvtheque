package fr.bestteam.cvtheque.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "formation")
public class Formation implements Serializable{
	/**
	 * Entit� Formation
	 */
	private static final long serialVersionUID = 1L;

	///////////////
	// Attributs
	///////////////
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(nullable = false)
	private String intitule;
	private String description;
	private String dateDebutFormation;
	private String dateFinFormation;
	@Column(nullable = false)
	private String nomCentreFormation;
	
	///////////////////
	// Jointures
	///////////////////
	@ManyToOne
	@JoinColumn(name = "idPersonne")
	private Personne personne;
	
	///////////////////
	// Constructeurs
	///////////////////
	public Formation() {
	}
	
	public Formation(String intitule, String description, String dateDebutFormation, String dateFinFormation,
			String nomCentreFormation) {
		super();
		this.intitule = intitule;
		this.description = description;
		this.dateDebutFormation = dateDebutFormation;
		this.dateFinFormation = dateFinFormation;
		this.nomCentreFormation = nomCentreFormation;
	}

	public Formation(String intitule, String nomCentreFormation) {
		super();
		this.intitule = intitule;
		this.nomCentreFormation = nomCentreFormation;
	}

	
	//////////////////////
	// GETTERS/SETTERS
	//////////////////////

	public Long getId() {
		return id;
	}


	public String getIntitule() {
		return intitule;
	}



	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public String getDateDebutFormation() {
		return dateDebutFormation;
	}



	public void setDateDebutFormation(String dateDebutFormation) {
		this.dateDebutFormation = dateDebutFormation;
	}



	public String getDateFinFormation() {
		return dateFinFormation;
	}



	public void setDateFinFormation(String dateFinFormation) {
		this.dateFinFormation = dateFinFormation;
	}



	public String getNomCentreFormation() {
		return nomCentreFormation;
	}



	public void setNomCentreFormation(String nomCentreFormation) {
		this.nomCentreFormation = nomCentreFormation;
	}
	
	public Personne getPersonne() {
		return personne;
	}



	public void setPersonne(Personne personne) {
		this.personne = personne;
	}


	//////////////////////
	// ToString
	//////////////////////


	@Override
	public String toString() {
		return "Experience [id=" + id + ", intitule=" + intitule + ", description=" + description
				+ ", dateDebutFormation=" + dateDebutFormation + ", dateFinFormation=" + dateFinFormation
				+ ", nomCentreFormation=" + nomCentreFormation + "]";
	}
	
}

