package fr.bestteam.cvtheque.bo;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * L'entit� Personne concerne ceux qui ont des CVs
 * @author CIVEL
 *
 */
@Entity
// JsonIdentityInfo : D�fini la table principale : �vite le bouclage JSON via le webService
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class)
@Table(name = "personne")
public class Personne implements Serializable{

	/**
	 * Entit� Personne
	 */
	private static final long serialVersionUID = 1L;

	///////////////
	// Attributs
	///////////////
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "nom", nullable = false)
	private String nom;
	@Column(name = "prenom", nullable = false)
	private String prenom;
	private String civilite;
	private String mail;
	@Column(name = "tel", length = 14)
	private String tel;
	private String adresse;
	private String ville;
	private String codePostal;
	private String dateNaissance;
	private String linkedIn;
	private String mobilite;
	private String permis;
	private String photo;
	private String cv;
	private String centreInteret;
	private String poste;
	private Boolean interne;

	
	///////////////
	// Jointures
	///////////////
	// Expériences
	@OneToMany(fetch = FetchType.EAGER, 
			mappedBy="personne",
			cascade = {CascadeType.REMOVE,CascadeType.PERSIST})
	@Fetch(value = FetchMode.SUBSELECT)
	private Set<Experience> experiences= new HashSet<>();
	
	// Formations
	@OneToMany(fetch = FetchType.EAGER, 
			mappedBy="personne",
			cascade = {CascadeType.REMOVE,CascadeType.PERSIST,CascadeType.REFRESH})
	@Fetch(value = FetchMode.SUBSELECT)
	private Set<Formation> formations= new HashSet<>();
	
	// Remarques
	@OneToMany(fetch = FetchType.EAGER, 
			mappedBy="personne",
			cascade = {CascadeType.REMOVE,CascadeType.PERSIST,CascadeType.REFRESH})
	@Fetch(value = FetchMode.SUBSELECT)
	private Set<Remarque> remarques= new HashSet<>();
	
	// Competences
	@ManyToMany(fetch = FetchType.LAZY,
			cascade = {CascadeType.PERSIST,CascadeType.REFRESH})
	
	@JoinTable(name="comp_pers",
			joinColumns = @JoinColumn(name="idpersonne"),
			inverseJoinColumns = @JoinColumn(name="idcompetence"))
	
	private Set<Competence> competences = new HashSet<>();
	
	// Mots cles
	@ManyToMany(fetch = FetchType.LAZY,
			cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
	
	@JoinTable(name="motcle_pers",
			joinColumns = @JoinColumn(name = "idpersonne"),
			inverseJoinColumns = @JoinColumn(name = "idmotcle"))
	
	private Set<MotCle> motscles = new HashSet<>();
	
	///////////////////
	// Constructeurs
	///////////////////
	public Personne() {
		
	}
	
	public Personne(String nom, String prenom, String civilite, String mail, String tel, String adresse, String ville,
			String codePostal,String dateNaissance, String linkedIn, String mobilite, String permis, String photo, String cv,
			String centreInteret, String poste, Boolean interne) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.civilite = civilite;
		this.mail = mail;
		this.tel = tel;
		this.adresse = adresse;
		this.ville = ville;
		this.codePostal=codePostal;
		this.dateNaissance = dateNaissance;
		this.linkedIn = linkedIn;
		this.mobilite = mobilite;
		this.permis = permis;
		this.photo = photo;
		this.cv = cv;
		this.centreInteret = centreInteret;
		this.poste = poste;
		this.interne = interne;
	}

	public Personne(String nom, String prenom, String civilite) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.civilite = civilite;
	}
	

	///////////////
	// Getters/Setters
	//////////////
	
	public Long getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getCivilite() {
		return civilite;
	}

	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(String dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getLinkedIn() {
		return linkedIn;
	}

	public void setLinkedIn(String linkedIn) {
		this.linkedIn = linkedIn;
	}

	public String getMobilite() {
		return mobilite;
	}

	public void setMobilite(String mobilite) {
		this.mobilite = mobilite;
	}

	public String getPermis() {
		return permis;
	}

	public void setPermis(String permis) {
		this.permis = permis;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getCv() {
		return cv;
	}

	public void setCv(String cv) {
		this.cv = cv;
	}

	public String getCentreInteret() {
		return centreInteret;
	}

	public void setCentreInteret(String centreInteret) {
		this.centreInteret = centreInteret;
	}

	public Set<Experience> getExperiences() {
		return experiences;
	}

	public void setExperiences(Set<Experience> experiences) {
		this.experiences = experiences;
	}

	public String getPoste() {
		return poste;
	}

	public void setPoste(String poste) {
		this.poste = poste;
	}

	public Boolean getInterne() {
		return interne;
	}

	public void setInterne(Boolean interne) {
		this.interne = interne;
	}
	
	public Set<Formation> getFormations() {
		return formations;
	}

	public void setFormations(Set<Formation> formations) {
		this.formations = formations;
	}
	
	public Set<Competence> getCompetences() {
		return competences;
	}

	public void setCompetences(Set<Competence> competences) {
		this.competences = competences;
	}
	
	public Set<MotCle> getMotscles() {
		return motscles;
	}
	
	public void setMotscles(Set<MotCle> motscles) {
		this.motscles = motscles;
	}
	
	public Set<Remarque> getRemarques() {
		return remarques;
	}
	
	public void setRemarques(Set<Remarque> remarques) {
		this.remarques = remarques;
	}
	
	///////////////
	// ToString
	//////////////

	@Override
	public String toString() {
		return "Personne [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", civilite=" + civilite + ", mail="
				+ mail + ", tel=" + tel + ", adresse=" + adresse + ", ville=" + ville + ", codePostal=" + codePostal
				+ ", dateNaissance=" + dateNaissance + ", linkedIn=" + linkedIn + ", mobilite=" + mobilite + ", permis="
				+ permis + ", photo=" + photo + ", cv=" + cv + ", centreInteret=" + centreInteret + ", poste=" + poste
				+ ", interne=" + interne + ", Liste d'experiences=" + experiences + ", Liste de formations=" + formations
				+ ", Liste de competences=" + competences + ", Liste de mots-clés=" + motscles + ", Liste des remarques=" + remarques + "]";
	}

	//////////////////
	// M�thodes
	/////////////////
	/**
	 * Ajout d'un experience
	 * @param experience	L'experience à ajouter
	 */
	public void addExperiencePersonne(Experience experience) {
		if (null==experience.getId()) {
			// Nouvelle experience
			this.experiences.add(experience);
			experience.setPersonne(this);
		}
	}

	/**
	 * Ajout d'une formation
	 * @param formation		La formation à ajouter
	 */
	public void addFormationPersonne(Formation formation) {
		if (null==formation.getId()) {
			// Nouvelle experience
			this.formations.add(formation);
			formation.setPersonne(this);
		}
	}
	
	public void addRemarque(Remarque remarque) {
		if (null==remarque.getId()) {
			// Nouvelle remarque
			this.remarques.add(remarque);
			remarque.setPersonne(this);
		}
	}
	
	/**
	 * Ajout d'une compétence
	 * @param competence	La compétence à ajouter
	 */
	public void addCompetence(Competence competence) {
			this.competences.add(competence);		
	}
	
	/**
	 * Ajout d'un mot-clé
	 * @param mot			Le mot-clé à ajouter
	 */
	public void addMotCle(MotCle mot) {
		this.motscles.add(mot);
	}
	
	/**
	 * Suppression d'une compétence
	 * @param competence	La compétence à supprimer
	 */
	public void deleteCompetence(Competence competence){
		for (Competence comp : this.competences) {
			if(competence.getId()==comp.getId()) {
				this.getCompetences().remove(comp);
			}
		}
	}
	
	/**
	 * Suppression d'un mot-clé
	 * @param mot		Le mot-clé à supprimer
	 */
	public void deleteMotCle(MotCle mot) {
		for (MotCle word: this.motscles) {
			if(mot.getId() == word.getId()) {
				this.getMotscles().remove(word);
			}
		}
	}

}
