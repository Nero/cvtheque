package fr.bestteam.cvtheque.bo;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "motcle")
public class MotCle implements Serializable {

	private static final long serialVersionUID = 1L;
	
	///////////////////////
	// Attributs
	///////////////////////
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(nullable = false)
	private String motCle;
	
	////////////////////
	// Jointures
	////////////////////
	@ManyToMany(mappedBy = "motscles")
	@JsonBackReference
	Set<Personne> personnes = new HashSet<>();
	
	///////////////////
	// Constructeurs
	///////////////////
	public MotCle() {
		super();
	}
	
	public MotCle(String motCle) {
		super();
		this.motCle = motCle;
	}

	
	/////////////////////
	// GETTERS / SETTERS
	/////////////////////
	public Long getId() {
		return id;
	}
	public String getMotCle() {
		return motCle;
	}
	public void setMotCle(String motCle) {
		this.motCle = motCle;
	}
	public Set<Personne> getPersonnes() {
		return personnes;
	}
	public void setPersonnes(Set<Personne> personnes) {
		this.personnes = personnes;
	}

	
	/////////////////////
	// ToString
	/////////////////////
	@Override
	public String toString() {
		return "MotCle [Id=" + id + ", motCle=" + motCle + "]";
	}
	

}
