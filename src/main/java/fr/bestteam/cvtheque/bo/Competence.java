package fr.bestteam.cvtheque.bo;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "competence")
public class Competence implements Serializable{

	/**
	 * Entit� Comp�tence s�rializ�e
	 */
	private static final long serialVersionUID = 1L;
	
	///////////////////////
	// Attributs
	///////////////////////
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(nullable = false)
	private String libelle;
	
	////////////////////
	// Jointures
	////////////////////
	@ManyToMany(mappedBy = "competences")
	@JsonBackReference
	Set<Personne> personnes = new HashSet<>();

	///////////////////
	// Constructeurs
	///////////////////
	public Competence() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Competence(String libelle) {
		super();
		this.libelle = libelle;
	}
	
	/////////////////////
	// GETTERS / SETTERS
	/////////////////////
	public Long getId() {
		return id;
	}


	public String getLibelle() {
		return libelle;
	}


	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}


	public Set<Personne> getPersonnes() {
		return personnes;
	}


	public void setPersonnes(Set<Personne> personnes) {
		this.personnes = personnes;
	}

	
	/////////////////////
	// ToString
	/////////////////////
	
	
	@Override
	public String toString() {
		return "Competence [id=" + id + ", libelle=" + libelle + "]";
	}
	
}
