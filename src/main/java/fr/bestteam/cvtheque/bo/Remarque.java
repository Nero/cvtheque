package fr.bestteam.cvtheque.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="remarque")
public class Remarque implements Serializable {

    private static final long serialVersionUID = 1L;
    
	///////////////
	// Attributs
	///////////////
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
    @Column(nullable = false)
    private String remarque;
    private String date;
    
    
	///////////////////
	// Jointures
	///////////////////
    @ManyToOne
    @JoinColumn(name="idPersonne")
    private Personne personne;
    
	///////////////////
	// Constructeurs
	///////////////////
	public Remarque() {
		super();
	}
	
	public Remarque(String remarque, String date) {
		super();
		this.remarque = remarque;
		this.date = date;
	}
	
	
	//////////////////////
	// GETTERS/SETTERS
	//////////////////////
	public Long getId() {
		return id;
	}
	public String getRemarque() {
		return remarque;
	}
	public void setRemarque(String remarque) {
		this.remarque = remarque;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Personne getPersonne() {
		return personne;
	}
	public void setPersonne(Personne pers) {
		this.personne = pers;
	}
	
	
	//////////////////////
	// ToString
	//////////////////////
	@Override
	public String toString() {
		return "Remarque [id=" + id + ", remarque=" + remarque + ", Date=" + date + "]";
	}
}
