package fr.bestteam.cvtheque.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import fr.bestteam.cvtheque.bo.Personne;
import fr.bestteam.cvtheque.bll.ManagerException;
import fr.bestteam.cvtheque.bll.PersonneManager;

@RestController
@RequestMapping("/cvtheque")
//@CrossOrigin(origins = "http://localhost:4200")
@CrossOrigin(origins = "http://localhost")
public class PersonneController {

	@Autowired
	PersonneManager manager;

	@GetMapping("/personne")
	public List<Personne> findAll() {
		return manager.findAll();
	}

	@GetMapping("/personne/{id}")
	public Personne getOnePersonneById(@PathVariable Long id) throws Exception {
	     return manager.findById(id);
	}

//	@PostMapping("/personne")
	//@PostMapping(value = "/personne", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@PostMapping(value = "/personne", consumes={"application/json"}, produces = {"application/json"})
	public Personne ajouterPersonne(@RequestBody Personne personne) {
		try {
			return manager.addPerson(personne);
//			return manager.addPerson(personne);
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
		return null;
	}
	
	@PutMapping("/personne")
	public void modifierPersonne(@RequestBody Personne personne) {
        try {
			manager.updatePerson(personne);
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
	}
	
	@DeleteMapping("/personne/{id}")
	public void supprimerPersonne(@PathVariable Long id) {
        try {
			manager.deleteById(id);
		} catch (ManagerException e) {
			System.err.println(e);
			e.printStackTrace();
		}
	}
}
