package fr.bestteam.cvtheque.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.jboss.logging.Logger;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
//@CrossOrigin(origins = "http://localhost:4200")
@CrossOrigin(origins = "http://localhost")
public class FileController {

	private static final Logger logger = Logger.getLogger(FileController.class.getName());
	
	@PostMapping("/upload/{type}")
	public ResponseEntity<String> uploadData(@RequestParam("file") MultipartFile file, @RequestParam("nomfichier") String nomfichier, @PathVariable String type) throws Exception {
		
		if (file == null) {
			throw new RuntimeException("Select a file");
		}
		
		InputStream inputStream = file.getInputStream();
		String originalName = file.getOriginalFilename();
		String name = file.getName();
		String contentType = file.getContentType();
		long size = file.getSize();
		
		logger.info("inputStream: " + inputStream);
		logger.info("originalName: " + originalName);
		logger.info("name: " + name);
		logger.info("contentType: " + contentType);
		logger.info("size: " + size);

		
		// nouveau test
	    File fileToSave = new File("C:\\xampp\\htdocs\\cvtheque\\"+type+"\\"+nomfichier);
	    fileToSave.createNewFile();
	    FileOutputStream fos = new FileOutputStream(fileToSave); 
	    fos.write(file.getBytes());
	    fos.close();
			
		return new ResponseEntity<String>(originalName, HttpStatus.OK);
	}
}
