package fr.bestteam.cvtheque.bll;

import java.util.List;

import fr.bestteam.cvtheque.bo.Experience;
import fr.bestteam.cvtheque.bo.Personne;

public interface PersonneManager {
	
	public Personne addPerson(Personne personne) throws ManagerException;
	public void updatePerson(Personne personne) throws ManagerException;
	public List<Personne> findAll();
	public Personne findById(Long id);
	public void deleteById(Long id) throws ManagerException;

}
