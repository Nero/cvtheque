package fr.bestteam.cvtheque.bll;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.bestteam.cvtheque.bo.Experience;

import fr.bestteam.cvtheque.dal.ExperienceDao;

@Service
public class ExperienceManagerImpl implements ExperienceManager{

	@Autowired
	ExperienceDao daoExp;
	
	@Override
	public void addExperience(Experience experience) throws ManagerException {
		if (null==experience.getIntitule()) {
			 throw new ManagerException("Fail to create : Null Intitule!");
		} else { 
			if (null==experience.getNomEntreprise()){
				throw new ManagerException("Fail to create : Null NomEntreprise!");
			}
			else { daoExp.save(experience);}
		}
		
	}

	@Override
	public void updateExperience(Experience experience) throws ManagerException {
		if (null==experience.getId()) {
			 throw new ManagerException("Fail to update : Null Id!");
		} else 
		{ daoExp.save(experience);}
		
	}

	@Override
	public List<Experience> findAll() {
		return (List<Experience>) daoExp.findAll();
	}

	@Override
	public Experience findById(Long id) {
		
		return daoExp.findById(id).get();
	}

	@Override
	public void deleteById(Long id) throws ManagerException {
		
		if (null == id || id <= 0) {
            throw new ManagerException("Fail to delete : ID is not valid!");
        }else {
        Experience experienceASupp = daoExp.findById(id).get();

        // delete it
        daoExp.deleteById(experienceASupp.getId());
        }
	}
		
}
