package fr.bestteam.cvtheque.bll;

import java.util.List;

import fr.bestteam.cvtheque.bo.Formation;

public interface FormationManager {

	public void addFormation(Formation formation) throws ManagerException;
	public void updateFormation(Formation formation) throws ManagerException;
	public List<Formation> findAll();
	public Formation findById(Long id);
	public void deleteById(Long id) throws ManagerException;
}
