package fr.bestteam.cvtheque.bll;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.bestteam.cvtheque.bo.Competence;
import fr.bestteam.cvtheque.dal.CompetenceDao;

@Service
public class CompetenceManagerImpl implements CompetenceManager{

	@Autowired
	CompetenceDao daoComp;
	
	@Override
	public void addCompetence(Competence competence) throws ManagerException {
		if (null==competence.getLibelle()) {
			throw new ManagerException("Fail to create : Null NomCompetence!");
		}
			else { daoComp.save(competence);
		}
	}
		
	

	@Override
	public void updateCompetence(Competence competence) throws ManagerException {
		if (null==competence.getId()) {
			throw new ManagerException("Competence not exist");
		} else {addCompetence(competence);}
		
	}

	@Override
	public void deleteCompetence(Long id) throws ManagerException {
		if (null == id || id <= 0) {
            throw new ManagerException("Fail to delete : ID is not valid!");
        } else {
	        // delete it
	        daoComp.deleteById(id);
        }
	}

	@Override
	public List<Competence> findAll() {
		return (List<Competence>) daoComp.findAll();
	}

	@Override
	public Competence findById(Long id) {
		return daoComp.findById(id).get();
	}

}
