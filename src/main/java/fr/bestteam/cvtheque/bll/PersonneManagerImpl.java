package fr.bestteam.cvtheque.bll;

import java.util.List;
import java.util.HashSet;
import java.util.Set;

import javax.transaction.Transactional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.bestteam.cvtheque.bo.Competence;
import fr.bestteam.cvtheque.bo.Experience;
import fr.bestteam.cvtheque.bo.Formation;
import fr.bestteam.cvtheque.bo.MotCle;
import fr.bestteam.cvtheque.bo.Personne;
import fr.bestteam.cvtheque.bo.Remarque;
import fr.bestteam.cvtheque.dal.CompetenceDao;
import fr.bestteam.cvtheque.dal.ExperienceDao;
import fr.bestteam.cvtheque.dal.FormationDao;
import fr.bestteam.cvtheque.dal.MotCleDao;
import fr.bestteam.cvtheque.dal.PersonneDao;
import fr.bestteam.cvtheque.dal.RemarqueDao;

@Service
public class PersonneManagerImpl implements PersonneManager{

	@Autowired
	PersonneDao dao;
	
	@Autowired
	ExperienceDao daoExp;
	
	@Autowired
	FormationDao daoFor;
	
	@Autowired
	CompetenceDao daoComp;
	
	@Autowired
	RemarqueDao daoRemq;
	
	@Autowired
	MotCleDao daoMot;
	
	/**
	 * Ajouter une personne
	 */	
	@Override
	@Transactional
	public Personne addPerson(Personne personne) throws ManagerException{

			if (null==personne.getNom()) {
				 throw new ManagerException("Fail to create : Null Nom!");
			} else
			if (null==personne.getPrenom()) {
					 throw new ManagerException("Fail to create : Null Prenom!");
			} else	{
				
			
				// Ins�rer les exp�riences
				for (Experience experience : personne.getExperiences()) {
					if (null==experience.getIntitule()) {
						throw new ManagerException("Fail to create : Null intitul�!");
					}else {
						if (null==experience.getNomEntreprise()){
							throw new ManagerException("Fail to create : Null NomEntreprise!");
						} else {
							daoExp.save(experience);
						}
					}
				}
				// Ins�rer les formations
				for (Formation formation : personne.getFormations()) {
					if (null==formation.getIntitule()) {
						throw new ManagerException("Fail to create : Null intitul�!");
					} else {
						if (null==formation.getNomCentreFormation()){
							throw new ManagerException("Fail to create : Null NomEntreprise!");
						} else {
							daoFor.save(formation);
						}
					}	
				}	
				// Ins�rer les remarques
				for (Remarque remq : personne.getRemarques()) {
					if (null==remq.getRemarque()) {
						throw new ManagerException("Fail to create : Remarque is null!");
					} else {
						daoRemq.save(remq);
					}	
				}	
				// Ins�rer les comp�tences
				for (Competence competence : personne.getCompetences()) {
					if (null==competence.getLibelle()) {
						throw new ManagerException("Fail to create : Null libell�!");
					} else {
						daoComp.save(competence);
					}	
				}
				// Ins�rer les mots-clés
				for (MotCle mot : personne.getMotscles()) {
					if (null==mot.getMotCle()) {
						throw new ManagerException("Fail to create : MotCle is null!");
					} else {
						daoMot.save(mot);
					}	
				}
				return dao.save(personne);
			}
	}
	
	/**
	 * Modifier la personne. Type Personne en entr�e
	 */
	@Override
	@Transactional
	public void updatePerson(Personne personne) throws ManagerException{
		if (null==personne.getId()) {
			 throw new ManagerException("Fail to update : Null Id!");
		} else {
			
			addPerson(personne);				
			}
		}
	/**
	 * Lister les personnes
	 */
	@Override
	public List<Personne> findAll() {

		return (List<Personne>) dao.findAll();
	}
	/**
	 * Trouver la personne par son Id type Long
	 */
	@Override
	public Personne findById(Long id) {

		return dao.findById(id).get();
	}
	/**
	 * Supprimer la personne par son Id type Long
	 */
	@Override
	@Transactional
	public void deleteById(Long id) throws ManagerException {
		 if (null == id || id <= 0) {
	            throw new ManagerException("Fail to delete : ID is not valid!");
	        }else {
	        Personne personneASupp = dao.findById(id).get();
	
	        // delete it
	        dao.deleteById(personneASupp.getId());
	        }
	}

	
}
