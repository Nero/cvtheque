package fr.bestteam.cvtheque.bll;

public class ManagerException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ManagerException(String message) {
		super(message);
	}

}
