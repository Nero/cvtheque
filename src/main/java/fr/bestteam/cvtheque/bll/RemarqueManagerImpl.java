package fr.bestteam.cvtheque.bll;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import fr.bestteam.cvtheque.bo.Remarque;
import fr.bestteam.cvtheque.dal.RemarqueDao;

public class RemarqueManagerImpl implements RemarqueManager {
	
	@Autowired
	RemarqueDao dao;

	@Override
	public void addRemarque(Remarque remarque) throws ManagerException {
		if (null==remarque.getRemarque() || "".equals(remarque.getRemarque())) {
			throw new ManagerException("Fail to create : Invalid Remarque!");
		} else {
			dao.save(remarque);
		}
	}

	@Override
	public void updateRemarque(Remarque remarque) throws ManagerException {
		if (null==remarque.getRemarque() || "".equals(remarque.getRemarque())) {
			throw new ManagerException("Fail to create : Invalid Remarque!");
		} else {
			addRemarque(remarque);
		}
	}

	@Override
	public List<Remarque> findAll() {
		return (List<Remarque>) dao.findAll();
	}

	@Override
	public void deleteRemarque(Long id) throws ManagerException {
		if (null == dao.findById(id)) {
			throw new ManagerException("Fail to create : Invalid Id !");
		} else {
			dao.deleteById(id);
		}
	}

	@Override
	public Remarque findById(Long id) {
		return dao.findById(id).get();
	}

}
