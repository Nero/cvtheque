package fr.bestteam.cvtheque.bll;

import java.util.List;

import fr.bestteam.cvtheque.bo.Remarque;

public interface RemarqueManager {

	/**
	 * Ajouter une remarque
	 * @param remarque		La remarque à ajouter
	 * @throws ManagerException
	 */
	public void addRemarque(Remarque remarque) throws ManagerException;
	
	/**
	 * Mettre à jour une remarque
	 * @param remarque		La remarque à mettre à jour
	 * @throws ManagerException
	 */
	public void updateRemarque(Remarque remarque) throws ManagerException;
	
	/**
	 * Récupérer la liste des remarques
	 * @return		La liste des remarques
	 */
	public List<Remarque> findAll();
	
	/**
	 * Supprimer une remarque depuis son identifiant
	 * @param id		L'identifiant de la remarque
	 * @throws ManagerException
	 */
	public void deleteRemarque(Long id) throws ManagerException;
	
	/**
	 * Récupérer une remarque depuis son identifiant
	 * @param id		L'identifiant de la remarque
	 * @return			La remarque trouvée
	 */
	public Remarque findById(Long id);
}
