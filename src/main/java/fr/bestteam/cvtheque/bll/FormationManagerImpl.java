package fr.bestteam.cvtheque.bll;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.bestteam.cvtheque.bo.Formation;
import fr.bestteam.cvtheque.dal.FormationDao;

@Service
public class FormationManagerImpl implements FormationManager{

	@Autowired
	FormationDao daoFor;
	
	@Override
	public void addFormation(Formation formation) throws ManagerException {
		if (null==formation.getIntitule()) {
			 throw new ManagerException("Fail to create : Null Intitule!");
		} else { 
			if (null==formation.getNomCentreFormation()){
				throw new ManagerException("Fail to create : Null NomCentreFormation!");
			}
			else { daoFor.save(formation);}
		}
		
	}

	@Override
	public void updateFormation(Formation formation) throws ManagerException {
		if (null==formation.getId()) {
			 throw new ManagerException("Fail to update : Null Id!");
		} else 
		{ daoFor.save(formation);}
		
	}

	@Override
	public List<Formation> findAll() {
		return (List<Formation>) daoFor.findAll();
	}

	@Override
	public Formation findById(Long id) {
		return daoFor.findById(id).get();
	}

	@Override
	public void deleteById(Long id) throws ManagerException {
		
		if (null == id || id <= 0) {
            throw new ManagerException("Fail to delete : ID is not valid!");
        }else {
        	daoFor.deleteById(id);
        }
	}

}
