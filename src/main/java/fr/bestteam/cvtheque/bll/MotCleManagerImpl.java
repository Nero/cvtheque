package fr.bestteam.cvtheque.bll;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.bestteam.cvtheque.bo.MotCle;
import fr.bestteam.cvtheque.dal.MotCleDao;

@Service
public class MotCleManagerImpl implements MotCleManager {
	
	@Autowired
	MotCleDao dao;

	@Override
	public void addMotCle(MotCle mot) throws ManagerException {
		if (null==mot.getMotCle()) {
			throw new ManagerException("Fail to create : Null mot-cle");
		} else {
			dao.save(mot);
		}
	}

	@Override
	public void updateMotCle(MotCle mot) throws ManagerException {
		if (null == mot.getId() || "".equals(mot.getMotCle())) {
			throw new ManagerException("Fail to update : invalid mot-cle");
		} else if (null == dao.findById(mot.getId())) {
			throw new ManagerException("Fail to update : Mot-cle not found in database");
		} else {
			addMotCle(mot);
		}
	}

	@Override
	public void deleteMotCle(Long id) throws ManagerException {
		if (null == id || id <= 0) {
			throw new ManagerException("Fail to delete : ID is not valid!");
		} else if (null == dao.findById(id)) {
			throw new ManagerException("Fail to delete : Mot-cle not found in database");
		} else {
			dao.deleteById(id);
		}
	}

	@Override
	public List<MotCle> findAll() {
		return (List<MotCle>) dao.findAll();
	}

	@Override
	public MotCle findBtId(Long id) {
		return dao.findById(id).get();
	}

}
