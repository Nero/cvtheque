package fr.bestteam.cvtheque.bll;

import java.util.List;


import fr.bestteam.cvtheque.bo.Experience;


public interface ExperienceManager {
	
	public void addExperience(Experience experience) throws ManagerException;
	public void updateExperience(Experience experience) throws ManagerException;
	public List<Experience> findAll();
	public Experience findById(Long id);
	public void deleteById(Long id) throws ManagerException;

}
