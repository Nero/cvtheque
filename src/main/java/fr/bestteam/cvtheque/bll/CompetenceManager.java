package fr.bestteam.cvtheque.bll;

import java.util.List;

import fr.bestteam.cvtheque.bo.Competence;


public interface CompetenceManager {
	
	public void addCompetence(Competence competence) throws ManagerException;
	public void updateCompetence(Competence competence) throws ManagerException;
	public void deleteCompetence(Long id) throws ManagerException;
	public List<Competence> findAll();
	public Competence findById(Long id);

}
