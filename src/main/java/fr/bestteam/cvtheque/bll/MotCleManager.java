package fr.bestteam.cvtheque.bll;

import java.util.List;

import fr.bestteam.cvtheque.bo.MotCle;

public interface MotCleManager {

	/**
	 * Ajout d'un mot cle
	 * @param mot		Le mot cle à ajouter
	 * @throws ManagerException
	 */
	public void addMotCle(MotCle mot) throws ManagerException;
	
	/**
	 * Mettre à jour un mot clé
	 * @param mot		Le mot clé à mettre à jour
	 * @throws ManagerException
	 */
	public void updateMotCle(MotCle mot) throws ManagerException;
	
	/**
	 * Supprimer un mot clé
	 * @param id		L'identifiant du mot clé à supprimer
	 * @throws ManagerException
	 */
	public void deleteMotCle(Long id) throws ManagerException;
	
	/**
	 * Récupérer la liste des mots-clés
	 * @return			La liste
	 */
	public List<MotCle> findAll();
	
	/**
	 * Récupérer un mot clé depuis son identifiant
	 * @param id		L'identifiant du mot-clé à récupérer
	 * @return			Le mot-clé
	 */
	public MotCle findBtId(Long id);
}
